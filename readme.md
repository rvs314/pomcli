# Pomcli - Commandline Pomodoro Utility

Hello! This is a really light pomodoro utility written in python. It uses the notify-send utility to send notifications to your system.

## Install

This program has only been tested on an Ubuntu 17.04 system, so I'm unsure if it'll work on other systems. I know it won't work with Windows devices and I haven't tested Macs (If anyone gets this to work please tell me, so I can update this). The only dependencies are Python 3+ and notify-system.

The best way to install this (at least on my system) is to move it to /usr/local/bin with the following steps:

1. Move the 'pomcli' file to /usr/local/bin with the command: "sudo mv pomcli /usr/local/bin"
1. Run the command "export PATH='/usr/local/bin:$PATH'" at terminal startup (by using .bashrc or otherwise)

## Usage

From help:

```
pomcli -h          Runs help (you're seeing this now)
pomcli -w 25       Sets amount of time for work (in minutes)
pomcli -sr 5       Sets amount of time for short rests (in minutes)
pomcli -lr 15      Sets amount of time for long rests (in minutes)
pomcli -ws wswswl  Sets work shedule using the schedule symbols
pomcli -d          Daemonize this program
pomcli             Equivilant to: pomcli -w 25 -sr 5 -lr 15 -ws wswswl

Schedule symbols:
w - one work cycle
s - one short rest
l - one long rest
```

## Misc.

If I messed up anywhere (which I'm sure I have), please inform me and I'll try to fix it right away. THX!
